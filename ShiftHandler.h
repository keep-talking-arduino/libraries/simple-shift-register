#ifndef ShiftHandler_H
#define ShiftHandler_H

#include <Arduino.h>

unsigned char ToByte(bool b[8]);

class ShiftHandler {

public:
  static ShiftHandler* getInstance();

  void configure(int clockPin, int latchPin, int dataPin);
  void shiftTo(int* [4][8], int regNr);
  int setValue(int regNr, int position, bool value);
  int setRegister(int regNr, bool value[8]);
  void update();
  void clear();
  void clear(int regNr);
  void printValues();
  
private:
  static ShiftHandler* _instance;
  ShiftHandler();

  int _clockPin;
  int _latchPin;
  int _dataPin;
  bool _shiftvalues [4][8];
};

#endif
