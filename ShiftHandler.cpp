#include <ShiftHandler.h>

/* converts booleanarray to byte */
unsigned int toByte(bool b[8])
{
    unsigned char c = 0;
    for (int i = 0; i < 8; ++i)
        if (b[i])
            c |= 1 << i;
    return c;
}

ShiftHandler* ShiftHandler::_instance = NULL;

ShiftHandler* ShiftHandler::getInstance(){
    if (_instance == NULL){ // prevents double initialization
        _instance = new ShiftHandler();
    }
    return _instance;
}

ShiftHandler::ShiftHandler(){}

/* configure output pins */
void ShiftHandler::configure(int clockPin, int latchPin, int dataPin){
  _clockPin = clockPin;
  _latchPin = latchPin;
  _dataPin = dataPin;

  pinMode(_clockPin, OUTPUT);
  pinMode(_latchPin, OUTPUT);
  pinMode(_dataPin, OUTPUT);

  _shiftvalues [4][8] = {false};
}

/* sets value of specified register on given pin */
int ShiftHandler::setValue(int regNr, int position, bool value){
  if(regNr > 3 || position > 8) return -1;
  _shiftvalues[regNr][position] = value;
  return 1;
}

/* sets all values of specified register at once */
int ShiftHandler::setRegister(int regNr, bool values[8]){
  if(regNr > 3) return -1;
  for(int i = 0; i < 8; i++){
    _shiftvalues[regNr][i] = values[i];
  }
  return 1;
}

/* sets all values of all registers to 0/false */
void ShiftHandler::clear(){
  for(int reg = 0; reg < 4; reg++){
    for(int bit = 0; bit < 8; bit++){
      _shiftvalues[reg][bit] = false;
    }
  }
}

/* sets all values of given register to 0/false */
void ShiftHandler::clear(int regNr){
  for(int bit = 0; bit < 8; bit++){
    _shiftvalues[regNr][bit] = false;
  }
}

/* outputs all values to shift registers */
void ShiftHandler::update(){
  digitalWrite(_latchPin, LOW);
  for(int i = 3; i >= 0; i--){
    shiftOut(_dataPin, _clockPin, MSBFIRST, toByte(_shiftvalues[i]));
  }
  digitalWrite(_latchPin, HIGH);
}

/* visualizes state of all bits by printing them to console */
void ShiftHandler::printValues(){
  for(int i = 0; i < 4; i++){
    for(int a = 0; a < 8; a++){
      if(_shiftvalues[i][a] == true){
        Serial.print("1");
      }else{
        Serial.print("0");
      }
      Serial.print(", ");
    }
    Serial.println("");
  }
}
